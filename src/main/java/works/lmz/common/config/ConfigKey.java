package works.lmz.common.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * new Configured annotation that provides key override for configuration
 */
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ConfigKey {
	/**
	 * If provided this is the key we look up in the configuration source
	 *
	 * @return key used for configuration source
	 */
	String value() default "";
}
