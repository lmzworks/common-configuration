package works.lmz.common.config;


import net.stickycode.configuration.ConfigurationKey;
import net.stickycode.configuration.ConfigurationSource;
import net.stickycode.configuration.ConfigurationValue;
import net.stickycode.configuration.ResolvedConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SystemPropertiesConfigurationSource implements ConfigurationSource {
	private Logger log = LoggerFactory.getLogger("works.lmz.configuration");

	@Override
	public void apply(ConfigurationKey configurationKey, ResolvedConfiguration resolvedConfiguration) {
		String key = configurationKey.join(".");

		final String value = System.getProperty(key);

		if (value != null) {
			resolvedConfiguration.add(new ConfigurationValue() {
				@Override
				public String get() {
					return value;
				}

				@Override
				public boolean hasPrecedence(ConfigurationValue v) {
					return false;
				}
			});
		}
	}
}
