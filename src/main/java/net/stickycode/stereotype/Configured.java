package net.stickycode.stereotype;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * author: Richard Vowles - http://gplus.to/RichardVowles
 *
 * This is a cheat to allow us to support the "old" @Configured element. without breaking anything.
 */
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Configured {
  /**
   * If provided this is the key we look up in the configuration source
   *
   * @return key used for configuration source
   */
  String value() default "";
}
